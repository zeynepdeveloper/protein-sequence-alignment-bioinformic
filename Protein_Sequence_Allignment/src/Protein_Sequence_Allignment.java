import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;


// Zeynep K�se

public class Protein_Sequence_Allignment {
	private static String blosum62;
	private static int m,n,g;
	private static String x,y;	
	private double gapCosts;
	private static int numberOfRows;
	private static int numberOfColumns;
	private static String alignedSequence1;
	private static String alignedSequence2;
	private static int scoreOfTheAlignment;
	private static int gapPenalty;
	static ArrayList<ArrayList> rows = new ArrayList<ArrayList>();
	static int[][] matrix={
			 { 4, -1, -2, -2,  0, -1, -1,  0, -2, -1, -1, -1, -1, -2, -1,  1,  0, -3, -2,  0, -2, -1,  0, -4},
		     {-1,  5,  0, -2, -3,  1,  0, -2,  0, -3, -2,  2, -1, -3, -2, -1, -1, -3, -2, -3, -1,  0, -1, -4},
			 {-2,  0,  6,  1, -3,  0,  0,  0,  1, -3, -3,  0, -2, -3, -2,  1,  0, -4, -2, -3,  3,  0, -1, -4},
			 {-2, -2,  1,  6, -3,  0,  2, -1, -1, -3, -4, -1, -3, -3, -1,  0, -1, -4, -3, -3,  4,  1, -1, -4},
			 { 0, -3, -3, -3,  9, -3, -4, -3, -3, -1, -1, -3, -1, -2, -3, -1, -1, -2, -2, -1, -3, -3, -2, -4},
			 {-1,  1,  0,  0, -3,  5,  2, -2,  0, -3, -2,  1,  0, -3, -1,  0, -1, -2, -1, -2,  0,  3, -1, -4},
			 {-1,  0,  0,  2, -4,  2,  5, -2,  0, -3, -3,  1, -2, -3, -1,  0, -1, -3, -2, -2,  1,  4, -1, -4},
			 { 0, -2,  0, -1, -3, -2, -2,  6, -2, -4, -4, -2, -3, -3, -2,  0, -2, -2, -3, -3, -1, -2, -1, -4},
			 {-2,  0,  1, -1, -3,  0,  0, -2,  8, -3, -3, -1, -2, -1, -2, -1, -2, -2,  2, -3,  0,  0, -1, -4},
			 {-1, -3, -3, -3, -1, -3, -3, -4, -3,  4,  2, -3,  1,  0, -3, -2, -1, -3, -1,  3, -3, -3, -1, -4},
			 {-1, -2, -3, -4, -1, -2, -3, -4, -3,  2,  4, -2,  2,  0, -3, -2, -1, -2, -1,  1, -4, -3, -1, -4},
			 {-1,  2,  0, -1, -3,  1,  1, -2, -1, -3, -2,  5, -1, -3, -1,  0, -1, -3, -2, -2,  0,  1, -1, -4},
			 {-1, -1, -2, -3, -1,  0, -2, -3, -2,  1,  2, -1,  5,  0, -2, -1, -1, -1, -1,  1, -3, -1, -1, -4},
			 {-2, -3, -3, -3, -2, -3, -3, -3, -1,  0,  0, -3,  0,  6, -4, -2, -2,  1,  3, -1, -3, -3, -1, -4},
			 {-1, -2, -2, -1, -3, -1, -1, -2, -2, -3, -3, -1, -2, -4,  7, -1, -1, -4, -3, -2, -2, -1, -2, -4},
			 { 1, -1,  1,  0, -1,  0,  0,  0, -1, -2, -2,  0, -1, -2, -1,  4,  1, -3, -2, -2,  0,  0,  0, -4},
			 { 0, -1,  0, -1, -1, -1, -1, -2, -2, -1, -1, -1, -1, -2, -1,  1,  5, -2, -2,  0, -1, -1,  0, -4},
			 {-3, -3, -4, -4, -2, -2, -3, -2, -2, -3, -2, -3, -1,  1, -4, -3, -2, 11,  2, -3, -4, -3, -2, -4},
			 {-2, -2, -2, -3, -2, -1, -2, -3,  2, -1, -1, -2, -1,  3, -3, -2, -2,  2,  7, -1, -3, -2, -1, -4},
			 { 0, -3, -3, -3, -1, -2, -2, -3, -3,  3,  1, -2,  1, -1, -2, -2,  0, -3, -1,  4, -3, -2, -1, -4},
			 {-2, -1,  3,  4, -3,  0,  1, -1,  0, -3, -4,  0, -3, -3, -2,  0, -1, -4, -3, -3,  4,  1, -1, -4},
			 {-1,  0,  0,  1, -3,  3,  4, -2,  0, -3, -3,  1, -1, -3, -1,  0, -1, -3, -2, -2,  1,  4, -1, -4},
			 { 0, -1, -1, -1, -2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -2,  0,  0, -2, -1, -1, -1, -1, -1, -4},
			 {-4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4,  1},
		 };
	
	

	
    private static int getIndex(char a) {
    	// check for upper and lowercase characters
    	switch ((String.valueOf(a)).toUpperCase().charAt(0)) {
	    	case 'A': return 0;
	    	case 'R': return 1;
	    	case 'N': return 2;
	    	case 'D': return 3;
	    	case 'C': return 4;
	    	case 'Q': return 5;
	    	case 'E': return 6;
	    	case 'G': return 7;
	    	case 'H': return 8;
	    	case 'I': return 9;
	    	case 'L': return 10;
	    	case 'K': return 11;
	    	case 'M': return 12;
	    	case 'F': return 13;
	    	case 'P': return 14;
	    	case 'S': return 15;
	    	case 'T': return 16;
	    	case 'W': return 17;
	    	case 'Y': return 18;
	    	case 'V': return 19;
	    	case 'B': return 20;
	    	case 'Z': return 21;
	    	case 'X': return 22;
	    	default: return 23;		// if not in the alphabet, go out of bounds
    	}
    }
    private static int getDistance(int i, int j)  {
    	if (i < 0 || i > matrix[0].length) {
    		System.out.println("Invalid amino acid character at string1, position " + i);
    	}
    	if (j < 0 || j > matrix[0].length) {
    		System.out.println("Invalid amino acid character at string2, position " + j);
    	}
    	
    	return matrix[i][j];
    }

    public static int getDistance(char a1, char a2)  {
    	// toUpper
    	return getDistance(getIndex(a1), getIndex(a2));  	
    }
    /**
     * Retrieves the score from the chosen substitution matrix.
     * @param i Internal index for the first amino acid
     * @param j	Internal index for the second amino acid
     * @return the score of the match/mismatch/gap
     */
    private double getScore(int i, int j) {
    	if (i < 0 || i >= matrix[0].length-1 || j < 0 || j >= matrix[0].length-1)
    		return gapCosts;	// if out of bounds we clearly have a gap!
    	return matrix[i][j];	// return the score otherwise
    }
    
	/**
	 * Sets the first row and the first column of the matrix
	 */	
	public static void setInitialMatrix()
	{
		for(int i=0; i<numberOfRows; i++)
		{
			ArrayList<MatrixElement> columns = new ArrayList<MatrixElement>();
			columns.add(new MatrixElement(i*gapPenalty, MatrixElement.ARROW_UP));
			rows.add(columns);
		}
		
		for(int j=1; j<=numberOfColumns; j++)
		{
			rows.get(0).add(new MatrixElement(j*gapPenalty,MatrixElement.ARROW_LEFT));
		}
		
		// Set the arrow of the beginning element of the matrix to NONE. 
		((MatrixElement) rows.get(0).get(0)).setArrow(MatrixElement.ARROW_NONE);
	}
	
	
	/**
	 * Fills the cells in the matrix with corresponding numbers and arrows
	 */
	
	public static void fillMatrix() 
	{
		for(int i=1; i<numberOfRows; i++)
		{
			for(int j=1; j<numberOfColumns; j++)
			{
				// Get the values of three neighbor cells
				int a = 0,b = 0,c = 0;
				

				a = ((MatrixElement) rows.get(i).get(j-1)).getValue() + gapPenalty;
				b = ((MatrixElement) rows.get(i-1).get(j)).getValue() + gapPenalty;
				
				c = ((MatrixElement) rows.get(i-1).get(j-1)).getValue() + getDistance(x.charAt(i-1), y.charAt(j-1));
				
			
				
				// Find the maximum number and put it in the appropriate cell
				if(a>b && a>=c)
				{
					rows.get(i).add(new MatrixElement(a, MatrixElement.ARROW_LEFT));
				}
				else if(b>c && b>=a)
				{
					rows.get(i).add(new MatrixElement(b, MatrixElement.ARROW_UP));
				}
				else if(c>a && c>=b)
				{
					rows.get(i).add(new MatrixElement(c, MatrixElement.ARROW_CROSS));
				}
				
			}
		}
		
	}
	
	
	/**
	 * Starts from the right bottom of the matrix and follows the arrows.
	 * Keeps the reversed alignments in char arrays.
	 */
	public static void traceBack()
	{
		StringBuilder builder1 = new StringBuilder();
		StringBuilder builder2 = new StringBuilder();
		
		int i = numberOfRows-1;
		int j = numberOfColumns-1;
		
		while(((MatrixElement) rows.get(i).get(j)).getArrow() != MatrixElement.ARROW_NONE)
		{
				// Set the letters with respect to the arrows
				if(((MatrixElement) rows.get(i).get(j)).getArrow() == MatrixElement.ARROW_UP)
				{
					builder2.append("-");
				}
				else
				{
					builder2.append(y.charAt(j-1));
				}
				
				if(((MatrixElement) rows.get(i).get(j)).getArrow() == MatrixElement.ARROW_LEFT)
				{
					builder1.append("-");
				}
				else
				{
					builder1.append(x.charAt(i-1));
				}
				
				
				// Set the indices
				if(((MatrixElement) rows.get(i).get(j)).getArrow() == MatrixElement.ARROW_CROSS)
				{
					i=i-1;
					j=j-1;
				}
				else if(((MatrixElement) rows.get(i).get(j)).getArrow() == MatrixElement.ARROW_LEFT)
				{
					j=j-1;
				}
				else if(((MatrixElement) rows.get(i).get(j)).getArrow() == MatrixElement.ARROW_UP)
				{
					i=i-1;
				}
		}
		
		// Put the results into the strings
		alignedSequence1 = builder1.reverse().toString();
		alignedSequence2 = builder2.reverse().toString();
	   
		scoreOfTheAlignment = ((MatrixElement) rows.get(numberOfRows-1).get(numberOfColumns-1)).getValue();
	}
	



	
	// you can add functions here�
	public static void main(String[] args) throws IOException{
	                    //your code starts from here�
		

    //inputs:
		/*
		x = "PLEASANTLY";
		y = "MEANLY";
		
		x = "MKTERPRPNTFIIRCLQWTTVIERTFHVETPEEREEWTTAIQTVADGLKKQEEEE";
		y = "CQLMKTERPRPNTFVIRCLQWTTVIERTFHVDSPDEREEWMRAIQMVANSLKQRGPGEDA";
		
		*/
		
		x = "PLEASANTLY";
		y = "MEANLY";

		
		m=x.length();
		n=y.length();
		

	    // define gap penalty
	    g = -5;
	    gapPenalty = g;
		numberOfRows = m+1;
		numberOfColumns = n+1;
	    //�.
	//output:
	// Print the maximum alignment score
	// Print the best alignment of given two sequences
		setInitialMatrix();
		System.out.println("Initial matrix has successfully set. \nComputation started...");
		
		fillMatrix();
		System.out.println("Matrix filled successfully. Reading the arrows backwards...");
	
		traceBack();
		
		System.out.println(alignedSequence1);
		System.out.println(alignedSequence2);
		System.out.println(scoreOfTheAlignment);
	}

}

class MatrixElement
{
	private int value;
	private int arrow;
	
	public final static int ARROW_LEFT = 1;
	public final static int ARROW_UP = 2;
	public final static int ARROW_CROSS = 3;	
	public final static int ARROW_NONE = -1;	
		
	public MatrixElement(int value, int arrow) {
		super();
		this.value = value;
		this.arrow = arrow;
	}
	
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	public int getArrow() {
		return arrow;
	}
	public void setArrow(int arrow) {
		this.arrow = arrow;
	}
	
	
	
	

}
